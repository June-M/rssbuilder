<?php 
// Include dependencies
require_once 'vendor/autoload.php';

// Create feed object from https://github.com/mibe/FeedWriter/
use \FeedWriter\RSS2;
$feed = new RSS2();

// Set some basic Feed channel info
$feed->setTitle("HWN News Network");
$feed->setLink("http://hwnmedia.com/");
$feed->setDescription("This feed provides article data to HWN screens.");
$feed->setDate(time());

// When adding images, the title (second argument) and link (third argument) must be the same as the channel title and link
$feed->setImage("http://google.ca/favicon.ico", "HWN News Network", "http://hwnmedia.com/");

// Use the setChannelElement() function for other optional channel elements.
// See http://www.rssboard.org/rss-specification#optionalChannelElements
// for other optional channel elements. Here the language code for American English is used for the optional "language" channel element.
$feed->setChannelElement("language", "en-US");
$feed->setChannelElement("pubDate", date(\DATE_RSS, strtotime("2013-04-06")));

// A few other things can be set (not all listed here)
$feed->setSelfLink("/rss.php");

// Now let's create an article...
$article = $feed->createNewItem();

// Set the basic article info
$article->setTitle("HWN News Network");
$article->setLink("http://hwnmedia.com/blog");
$article->setDescription("Checkout our first post here!");
$article->setDate("2017-06-01 00:50:30");

// Add the article into the feed...
$feed->addItem($article);

// Two ways to use the feed - generate it and store it in memory
$feedStr = $feed->generateFeed();

// Or print it directly to the output
$feed->printFeed();

// So ideally we just pull articles from a database, create articles in a loop, load all the properties into the right spots
// and we're good to go! 

?>
